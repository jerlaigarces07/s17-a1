let students = []

function addStudent(name){
	students.push(name)
	console.log(name + " was added to the student's list.");

}
function countStudents() {
	console.log('There are a total of ' + students.length + ' students enrolled.')
}

function printStudents() {
	students.forEach(function(students){
		console.log(students)
	})
}

function findStudent(name) {
	let result = students.filter(function(student){
		return (student.toLowerCase() == name.toLowerCase());
	})
	let count = result.length
	let message = ''

	if (count == 1) {
		message = ' is an Enrollee'
		console.log(result + message)
	} else if (count > 1) {
		message = ' are enrollees'
		console.log(result + message)
	} else {
		message = 'No student found with the name ' + name
		console.log(message)
	}
}

function removeStudent(name){
	var index = students.indexOf(name);
	if (index !== -1) {
	  students.splice(index, 1);
	  console.log(name + " was removed from the student's list.")
	}
}